#!/bin/sh

WRKINVENTORY="/tmp/add-ansible.$$"

echo -n "Enter host you want to add Ansible to : "
read HOST
echo ${HOST} > ${WRKINVENTORY}

echo -n "Enter userid on ${HOST} that has sudo priveleges (or root) to : "
read USER

echo -n "enter password for user: ${USER} : "
stty_orig=`stty -g`
stty -echo
read PWD
stty $stty_orig
echo

echo -n "enter a password for user: ansible : "
stty -echo
read PLAINPWD
stty $stty_orig
echo
ANSPWD=`openssl passwd -6 "${PLAINPWD}"`

# add user
ansible ${HOST} -i ${WRKINVENTORY} -m user -a "name=ansible create_home=yes password='${ANSPWD}'" -b --extra-vars "ansible_user=${USER} ansible_password='${PWD}'"

# add sudoers.d file
ansible ${HOST} -i ${WRKINVENTORY} -m copy -a "content='ansible ALL=(ALL) NOPASSWD: ALL' dest=/etc/sudoers.d/ansible" -b --extra-vars "ansible_user=${USER} ansible_password='${PWD}'"

# add ssh key
ansible ${HOST} -i ${WRKINVENTORY} -m authorized_key -a "user=ansible key=\"{{ lookup('file', '/home/ansible/.ssh/id_rsa.pub') }}\" state=present" -b --extra-vars "ansible_user=${USER} ansible_password='${PWD}'"

rm ${WRKINVENTORY}
